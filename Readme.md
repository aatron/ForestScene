# Forest Scene

 ![Logo](/docs/img/ForestScene_Small.jpg)

The logo and namesake of this project are from Johann Hermann Carmiencke's piece called [Forest Scene](http://www.metmuseum.org/art/collection/search/10378?sortBy=Relevance&amp;ft=tree+sketch+branch&amp;offset=300&amp;rpp=100&amp;pos=348). Thanks to [The Met](http://www.metmuseum.org) for hosting their [collection](http://www.metmuseum.org/art/collection) online.